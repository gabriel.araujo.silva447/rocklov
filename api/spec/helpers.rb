# frozen_string_literal: true

module Helpers
  def get_fixture(item)
    YAML.safe_load(File.read(Dir.pwd + "/spec/fixtures/#{item}.yml"), symbolize_names: true)
  end

  def get_thumb(file_name)
    File.open(File.join(Dir.pwd, 'spec/fixtures/images', file_name), 'rb')
  end

  module_function :get_fixture
  module_function :get_thumb
end

# module Helpers_cadastro
#    def get_cadastro(nome)
#      YAML.safe_load(File.read(Dir.pwd + "/spec/fixtures/#{nome}.yml"), symbolize_names: true)
#    end
#   module_function :get_cadastro
# end
