# frozen_string_literal: true

describe 'Post /sessions' do
  context 'login com sucesso' do
    before(:all) do
      payload = { email: 'betao@hotmail.com', password: 'pwd123' }
      @result = Sessions.new.login(payload)
    end
    it 'valida status code' do
      expect(@result.code).to eql 200
    end

    it 'valida id do usuario' do
      expect(@result.parsed_response['_id'].length) .to eql 24
    end
  end

  # examples = [
  #   {
  #     title: 'senha incorreta',
  #     payload: { email: 'Gabriel.araujo@gmail.com', password: 'pwd1234' },
  #     code: 401,
  #     error: 'Unauthorized'
  #   },
  #   {
  #     title: 'email incorreta',
  #     payload: { email: '234@gmail.com', password: 'pwd1234' },
  #     code: 401,
  #     error: 'Unauthorized'
  #   },
  #   {
  #     title: 'email em branco',
  #     payload: { email: '', password: 'pwd1234' },
  #     code: 412,
  #     error: 'required email'
  #   },
  #   {
  #     title: 'sem email',
  #     payload: { password: 'pwd1234' },
  #     code: 412,
  #     error: 'required email'
  #   },
  #   {
  #     title: 'senha em branco',
  #     payload: { email: 'Gabriel.araujo@gmail.com', password: '' },
  #     code: 412,
  #     error: 'required password'
  #   },
  #   {
  #     title: 'sem senha',
  #     payload: { email: 'Gabriel.araujo@gmail.com' },
  #     code: 412,
  #     error: 'required password'
  #   }
  # ]
  examples = Helpers.get_fixture('login')

  examples.each do |_e|
    context (_e[:title]).to_s do
      before(:all) do
        @result = Sessions.new.login(_e[:payload])
      end
      it "valida status code #{_e[:code]}" do
        expect(@result.code).to eql _e[:code]
      end

      it 'valida id do usuário' do
        expect(@result.parsed_response['error']) .to eql _e[:error]
      end
    end
  end
end
