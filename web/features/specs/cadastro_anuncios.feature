#language: pt

Funcionalidade: Cadastro de Anúncios
  Sendo usuário cadastrado no Rocklov que possui equipamentos musicais
  Quero cadastrar meus equipamentos
  Para que eu possa disponibilizados para locação

  Contexto: Login
    * Login com "cici@gmail.com" e "pwd123"

  Cenario: Novo equipo

    Dado que acesso o formulario de cadastro de anuncios
    E que eu tenho o seguinte equipamento:
      | thumb     | fender-sb.jpg |
      | nome      | Fender Strato |
      | categoria | Cordas        |
      | preco     | 200           |
    Quando submeto o cadastro desse item
    Então devo ver esse item no meu Dashboard

  Esquema do Cenario: Tentativa de cadastro de anúncios

    Dado que acesso o formulario de cadastro de anuncios
    E que eu tenho o seguinte equipamento:
      | thumb     | <foto>      |
      | nome      | <nome>      |
      | categoria | <categoria> |
      | preco     | <preco>     |
    Quando submeto o cadastro desse item
    Então deve conter a mensagem de alerta: "<saida>"

    Exemplos:
      | foto          | nome            | categoria | preco | saida                                |
      |               | violao de nylon | Cordas    | 150   | Adicione uma foto no seu anúncio!    |
      | clarinete.jpg |                 | Outros    | 100   | Informe a descrição do anúncio!      |
      | violino.jpg   | violino         | Cordas    |       | Informe o valor da diária            |
      | clarinete.jpg | clarinete       |           | 200   | Informe a categoria                  |
      | clarinete.jpg | clarinete       | Cordas    | ioo   | O valor da diária deve ser numérico! |
      | clarinete.jpg | clarinete       | Cordas    | 100a  | O valor da diária deve ser numérico! |