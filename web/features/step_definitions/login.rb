# frozen_string_literal: true

Dado('que acesso a página principal') do
  @login_page.open
end

Quando('submeto minhas credenciais {string} e {string}') do |_email, _password|
  @login_page.with(_email, _password)
end
