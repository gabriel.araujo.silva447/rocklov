# frozen_string_literal: true

class LoginPage
  include Capybara::DSL

  def open
    visit '/'
  end

  def with(_email, _password)
    find("input[placeholder='Seu email']").set _email
    find('input[type=password]').set _password
    click_button 'Entrar'
  end
end